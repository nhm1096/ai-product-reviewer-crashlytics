/**
 * Import function triggers from their respective submodules:
 *
 * const {onCall} = require("firebase-functions/v2/https");
 * const {onDocumentWritten} = require("firebase-functions/v2/firestore");
 *
 * See a full list of supported triggers at https://firebase.google.com/docs/functions
 */

const admin = require("firebase-admin");
const { onRequest } = require("firebase-functions/v2/https");
const logger = require("firebase-functions/logger");
const functions = require("firebase-functions");
const { StatusCodes } = require("http-status-codes");

// Moments library to format dates.
const moment = require("moment");
const config = require("./src/config");

// Initialize Functions
admin.initializeApp();

// Firebase crash detector
const {
  onNewFatalIssuePublished,
  onRegressionAlertPublished,
  onVelocityAlertPublished,
  onNewNonfatalIssuePublished,
} = require("firebase-functions/v2/alerts/crashlytics");

const URL =
  "https://aa7d-118-69-133-162.ngrok-free.app/webhook/event/firebase/alerts";

const fetch = require("node-fetch");
const requestPromise = require("request-promise-native");

/**
 * Posts a message to Discord with Discord's Webhook API
 *
 * @param {string} message - The message to post
 */
async function postMessageWebhook(message) {
  const webhookUrl = URL;
  if (!webhookUrl) {
    throw new Error("No webhook URL found.");
  }

  return fetch(webhookUrl, {
    contentType: "application/json",
    muteHttpExceptions: true,
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(message),
  });
}

exports.postfatalissuewebhook = onNewFatalIssuePublished(async (event) => {
  try {
    await postMessageWebhook({
      message: JSON.stringify(event),
    });

    logger.info(`Posted fatal Crashlytics alert to Webhook`);
  } catch (error) {
    logger.error(`Unable to post fatal Crashlytics alert to Webhook`, error);
  }
});

exports.postregressionissuewebhook = onRegressionAlertPublished(
  async (event) => {
    try {
      await postMessageWebhook({
        message: JSON.stringify(event),
      });

      logger.info(`Posted regression Crashlytics alert to Webhook`);
    } catch (error) {
      logger.error(`Unable to post fatal Crashlytics alert to Webhook`, error);
    }
  }
);

exports.postvelocityissuewebhook = onVelocityAlertPublished(async (event) => {
  try {
    await postMessageWebhook({
      message: JSON.stringify(event),
    });

    logger.info(`Posted velocity Crashlytics alert to Webhook`);
  } catch (error) {
    logger.error(`Unable to post fatal Crashlytics alert to Webhook`, error);
  }
});

exports.postnonfatalissuewebhook = onNewNonfatalIssuePublished(
  async (event) => {
    try {
      await postMessageWebhook({
        message: JSON.stringify(event),
      });

      logger.info(`Posted nonfatal Crashlytics alert to Webhook`);
    } catch (error) {
      logger.error(`Unable to post fatal Crashlytics alert to Webhook`, error);
    }
  }
);

// Demo Function API
exports.reportCrash = functions.https.onRequest(async (req, res) => {
  if (req.method !== "POST") {
    return res.status(StatusCodes.FORBIDDEN).send("Forbidden!");
  }
  logger.info(`Posted reportCrash to Webhook`);

  await postMessageWebhook({
    message: "test thu nha",
  });
  res.status(StatusCodes.OK).send("OK!");
});
