const parseObjectResult = (resultArr) => {
  const obj = {};
  const results = resultArr[0];
  Object.keys(results).forEach((key) => {
    let row = results[key];
    obj[key] = row;
  });
  return obj;
}

module.exports = {
  parseObjectResult,
};
